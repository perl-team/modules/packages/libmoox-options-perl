libmoox-options-perl (4.103-5) unstable; urgency=medium

  * Team upload.
  * Move libmoox-configfromfile-perl from Depends to Recommends.
    Thanks to intrigeri for the bug report. (Closes: #1090734)
  * Declare compliance with Debian Policy 4.7.0.
  * Update alternative (build) dependencies.
  * debian/watch: use uscan macros.
  * Set Rules-Requires-Root: no.
  * debian/upstream/metadata: remove obsolete Contact and Name fields.

 -- gregor herrmann <gregoa@debian.org>  Wed, 18 Dec 2024 17:57:41 +0100

libmoox-options-perl (4.103-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Update standards version to 4.5.0, no changes needed.
  * Remove constraints unnecessary since buster
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 30 Jun 2022 20:48:15 +0100

libmoox-options-perl (4.103-3) unstable; urgency=medium

  * Team upload.
  * Make build dependency on librole-tiny-perl versioned.
  * Drop Adjust-for-Role-Tiny-2.001003-test-suite-and-namespace-cl.patch.

 -- gregor herrmann <gregoa@debian.org>  Fri, 25 Oct 2019 20:32:42 +0200

libmoox-options-perl (4.103-2) unstable; urgency=medium

  * Team upload
  * New patch: mitigate the impact of regressions caused
    by librole-tiny-perl 2.001003 (workarounds #942275)

 -- intrigeri <intrigeri@debian.org>  Mon, 14 Oct 2019 09:43:48 +0000

libmoox-options-perl (4.103-1) unstable; urgency=medium

  [ Jonas Smedegaard ]
  * Fix spelling error in long description.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update URLs from {search,www}.cpan.org to MetaCPAN.
  * debian/*: update GitHub URLs to use HTTPS.

  [ Florian Schlichting ]
  * Import upstream version 4.103
  * Add upstream metadata file
  * Port packaging from cdbs to dh 11
  * Drop 2001_relax_getopt-long_dep.patch, new enough Getopt::Long is now
    available
  * Use https for copyright format URL
  * Add myself to Uploaders and Copyright
  * Update copyright holders and years
  * Update (build-)dependencies
  * Drop unused lintian override
  * Declare compliance with Debian Policy 4.3.0

 -- Florian Schlichting <fsfs@debian.org>  Thu, 27 Dec 2018 00:52:44 +0100

libmoox-options-perl (4.023-1) unstable; urgency=medium

  [ upstream ]
  * New release.
    + Fix error reporting on missing required arguments with Moo
      2.002002.
    + Remove perl 5.10 deps.

  [ Jonas Smedegaard ]
  * Update watch file:
    + Bump to file format 4.
    + Watch only MetaCPAN URL.
    + Mention gbp --uscan in usage comment.
  * Declare compliance with Debian Policy 3.9.8.
  * Modernize Vcs-Git field to use https URL.
  * Modernize git-buildpackage config: Filter any .git* file.
  * Update copyright info: Extend coverage for Debian packaging.
  * Drop patch about error reporting adopted upstream.
  * Unfuzz patch 2001.
  * Add README to source documenting patch naming micro policy.
  * Modernize CDBS use: Build-depend on licensecheck (not devscripts).

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 15 Dec 2016 04:20:13 +0100

libmoox-options-perl (4.022-2) unstable; urgency=medium

  * Team upload.
  * Fix-error-reporting-on-missing-required-arguments-wi.patch:
    new patch, for compatibility with libmoo-perl 2.002002 and newer
    (Closes: #829549).
  * Enable the autopkgtest-pkg-perl test suite, and:
    - disable use.t: this Moo role cannot be "used" this way
    - include the "etc", that some tests need

 -- intrigeri <intrigeri@debian.org>  Tue, 12 Jul 2016 17:22:22 +0000

libmoox-options-perl (4.022-1) unstable; urgency=medium

  [ upstream ]
  * New release.
    + Remove useless space.

  [ Jonas Smedegaard ]
  * Unfuzz patch 2001.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 08 Dec 2015 02:05:34 +0530

libmoox-options-perl (4.021-1) unstable; urgency=medium

  [ upstream ]
  * New release.
    + Translate using Locale::TextDomain.
    + Add setlocale for linux system.

  [ Jonas Smedegaard ]
  * Unfuzz patch 2001.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 23 Nov 2015 14:22:51 +0100

libmoox-options-perl (4.020-2) unstable; urgency=medium

  * Update package relations: Really recommend libintl-xs-perl (as intended in 4.020-1).

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 09 Nov 2015 18:19:18 +0100

libmoox-options-perl (4.020-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).
    + Handle multiple autosplit options correctly.
    + Add compact help.
    + Translate using Locale::TextDomain.
    + Reduce runtime dependency footprint.
    + Add spacer option.
    + Use "format" => "json" instead of json => 1.
    + Fix LC_ALL in tests.

  [ Jonas Smedegaard ]
  * Update package relations:
    + (Build-)depend on libjson-maybexs-perl (not libjson-perl).
    + (Build-)depend on liblocale-textdomain-perl libterm-size-any-perl.
    + Stop (build-)depend on libmro-compat-perl.
  * Modernize git-buildpackage config: Avoid git- prefix.
  * Update copyright info:
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
    + Extend copyright of packaging to cover current year.
  * Add lintian override regarding license in License-Reference field.
    See bug#786450.
  * Bump debhelper compatibility level to 9.
  * Add lintian override regarding debhelper 9.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 09 Nov 2015 11:19:35 +0100

libmoox-options-perl (4.018-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).
    + New feature Autorange.
    + Fix warning generated when missing required params.
    + Fix recommend Term::Any::Size.
    + Add "hidden" attribute, and doc.
    + Improve error message for isa check failures.
    + Fix upgrade deps on Module::Build and Getopt::Long.
    + Add usage_string parameters.

  [ Jonas Smedegaard ]
  * Update package relations:
    + Build-depend on recent libmodule-build-perl.
    + Tighten (build-)dependency on libgetopt-long-descriptive-perl.
    + Relax to recommend (not depend on) libterm-size-any-perl.
  * Add patch 2001 to relax dependency on Getopt::Long (and hope it
    wasn't important).

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 03 May 2015 00:42:12 +0200

libmoox-options-perl (4.012-1) unstable; urgency=medium

  [ upstream ]
  * New release.
    + Fix accept "0" as value for options with format "s".
    + Use Text::LineFold instead of Text::WrapI18N.
    + Support UTF8 source for pod and man.
    + Fix don't change option of options for trait.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to use cgit web frontend.

  [ Jonas Smedegaard ]
  * Declare compliance with Debian Policy 3.9.6.
  * Update package relations:
    + (Build-)depend on libunicode-linebreak-perl (not
      libtext-wrapi18n-perl).
    + Update to favor (build-)dependency on (now released) recent perl
      over recent libmodule-metadata-perl.
    + Build-depend on libmoox-cmd-perl: Strengthens testsuite.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 14 Oct 2014 18:08:33 +0200

libmoox-options-perl (4.008-2) unstable; urgency=medium

  * Fix build-depend explicitly on libmodule-build-perl.
  * Update watch file to use www.cpan.org URL, and switch to https-based
    metacpan.org URL.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 02 Jul 2014 18:17:56 +0200

libmoox-options-perl (4.008-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ Jonas Smedegaard ]
  * Bump to standards-version 3.9.5.
  * Fix use canonical Vcs-Git URL.
  * Update copyright info:
    + Bump (yes, not extend) coverage for upstream author.
    + Extend coverage of packaging.
    + Update preferred upstream contact.
  * Update watch file to use metacpan.org URL.
  * Update package relations:
    + (Build-)depend on libmoox-configfromfile-perl,
      libterm-size-any-perl and libtext-wrapi18n-perl, and on recent
      libpath-class-perl.
    + Tighten (build-)dependency on libmodule-metadata-perl.
    + Fix tighten to (build-)depend versioned on libmoo-perl.
    + Stop (build-)depending on libmodule-load-conditional-perl.
    + Build-depend on libcapture-tiny-perl.
    + Fix build-depend on (recent perl or) recent libtest-simple-perl.
    + Stop build-depending on libscalar-list-utils-perl.
  * Update short description, based on upstream info.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 31 May 2014 01:02:08 +0200

libmoox-options-perl (3.83-1) unstable; urgency=low

  [ upstream ]
  * New release.
    * Use GRS for release.
    * Fix README.mkdn.

  [ Jonas Smedegaard ]
  * Update alternate git source, Upstream-Contact and Homepage URLs.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 16 Jul 2013 15:46:13 +0200

libmoox-options-perl (3.80-2) unstable; urgency=low

  * Team upload.
  * Add alternative (build) dependencies for dual-lifed modules.
    (Closes: #710952)

 -- gregor herrmann <gregoa@debian.org>  Mon, 03 Jun 2013 21:50:35 +0200

libmoox-options-perl (3.80-1) unstable; urgency=low

  [ upstream ]
  * New release.
    + Add JSON support.
    + Fix pod issue.

  [ Jonas Smedegaard ]
  * Add README.source emphasizing control.in file as *not* a
    show-stopper for contributions, referring to wiki page for details.
  * Update package relations:
    + (Build-)depend on libjson-perl.
    + Build-depend on libtest-requires-perl.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 06 May 2013 14:18:25 +0200

libmoox-options-perl (3.78-1) unstable; urgency=low

  [ upstream ]
  * New release.
    + Support for preferred commandline.

  [ Jonas Smedegaard ]
  * Update copyright file: List github as alternate source.
  * Stop track md5sum of upstream tarball.
  * Stop track upstream development releases.
  * Fix stop build-depend on libtest-simple-perl: Provided by perl.
  * Have git-buildpackage ignore upstream .gitignore files.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 12 Apr 2013 21:25:56 +0200

libmoox-options-perl (3.77-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#703502.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 20 Mar 2013 13:09:12 +0100
